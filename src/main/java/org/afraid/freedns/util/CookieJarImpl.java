package org.afraid.freedns.util;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * cookie处理
 * 
 * @author jianggq
 * @date 2018年7月24日下午8:11:13
 */
@Slf4j
public class CookieJarImpl implements CookieJar {
	private CookieStore cookieStore;

	public CookieJarImpl(CookieStore cookieStore) {

		if (cookieStore == null)
			throw new IllegalArgumentException("cookieStore can not be null.");
		this.cookieStore = cookieStore;
	}

	@Override
	public synchronized void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
		log.info("保存的cookies：{} >> {} ", url.toString(), cookies);
		cookieStore.add(url, cookies);
	}

	@Override
	public synchronized List<Cookie> loadForRequest(HttpUrl url) {
		log.info("获取的cookies：{} >> {} ", url.toString(), cookieStore.get(url));
		return cookieStore.get(url);
	}

	public CookieStore getCookieStore() {
		return cookieStore;
	}
}
