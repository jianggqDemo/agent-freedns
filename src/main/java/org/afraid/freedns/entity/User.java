package org.afraid.freedns.entity;

import java.util.Date;
import javax.persistence.*;

/**
 * 用户实体
 * 
 * @author windows
 * @date 2018年7月24日下午7:37:25
 */
public class User {
	/**
	 * 主键
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Long id;

	/**
	 * 用户账号
	 */
	private String account;

	/**
	 * 用户名称
	 */
	private String name;

	/**
	 * 用户密码
	 */
	private String pwd;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 获取主键
	 *
	 * @return id - 主键
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置主键
	 *
	 * @param id
	 *            主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取用户账号
	 *
	 * @return account - 用户账号
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * 设置用户账号
	 *
	 * @param account
	 *            用户账号
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * 获取用户名称
	 *
	 * @return name - 用户名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置用户名称
	 *
	 * @param name
	 *            用户名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取用户密码
	 *
	 * @return pwd - 用户密码
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * 设置用户密码
	 *
	 * @param pwd
	 *            用户密码
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * 获取用户邮箱
	 *
	 * @return email - 用户邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置用户邮箱
	 *
	 * @param email
	 *            用户邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 获取更新时间
	 *
	 * @return update_time - 更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置更新时间
	 *
	 * @param updateTime
	 *            更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 获取创建时间
	 *
	 * @return create_time - 创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createTime
	 *            创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}