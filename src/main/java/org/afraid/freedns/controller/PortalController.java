package org.afraid.freedns.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.afraid.freedns.dto.UserDto;
import org.afraid.freedns.dto.UserLoginDto;
import org.afraid.freedns.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

/**
 * 主页控制层
 * 
 * @author jianggq
 * @date 2018年7月24日下午6:46:35
 */
@Controller
@Slf4j
public class PortalController {

	private final UserService userService;

	private final UserController userController;

	@Autowired
	public PortalController(UserService userService, UserController userController) {
		this.userService = userService;
		this.userController = userController;
	}

	/**
	 * 主页跳转<br/>
	 * 
	 * 1：如果session不为空，则跳转到登录后的主页，由ajax去获取User信息<br/>
	 * 2：如果session为空并且数据库里有一条记录，则默认根据这条记录登录。<br/>
	 * 3：如果session和数据库都为空，则跳转到登录界面
	 * 
	 * 需要考虑，OKHttp工具类会操作用户信息失败中，http://freedns.afraid.org网session过期，这时需要返回给前端界面，界面重新登录或直接跳转。
	 * 
	 * @param loginDto
	 * @return
	 */
	@GetMapping(value = { "", "/" })
	public ModelAndView profile(HttpServletRequest request) {

		HttpSession session = request.getSession(false);// 如果 true 返回新的Session； 如果 false，那么返回 null。
		UserDto userDto = userService.getOnlyOne(); // 因为是演示demo，这里只查询一条记录

		if (userDto == null) {
			log.info("本地没有一条记录");
			return new ModelAndView("ftl/login");
		}

		if (session == null) {
			log.info("本地有数据，session为空，则自动登录");

			// 2：如果session为空并且数据库里有一条记录，则默认根据这条记录登录。<br/>
			UserLoginDto loginDto = new UserLoginDto();
			loginDto.setEmail(userDto.getEmail());
			loginDto.setPwd(userDto.getPwd());

			return userController.login(loginDto, request);
		}

		return new ModelAndView("ftl/profile", "user", userDto);
	}

}
