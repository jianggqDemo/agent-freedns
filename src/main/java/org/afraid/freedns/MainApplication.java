package org.afraid.freedns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tk.mybatis.spring.annotation.MapperScan;

/**
 * 
 * SpringBoot主启动类
 * 
 * @author jianggq
 * @date 2018年7月24日下午5:32:16
 */
@MapperScan("org.afraid.freedns.mapper")
@SpringBootApplication
public class MainApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

}
