package org.afraid.freedns.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * API调用异常
 * 
 * @author jianggq
 * @date 2018年7月24日下午8:42:56
 */
@Getter
@Setter
public class APIException extends RuntimeException {

	private static final long serialVersionUID = -5912313864820593490L;

	public APIException(String message) {
		super(message);
	}

	public APIException(Throwable cause) {
		super(cause);
	}

	public APIException(String message, Throwable cause) {
		super(message, cause);
	}

}
