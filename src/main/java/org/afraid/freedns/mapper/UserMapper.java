package org.afraid.freedns.mapper;

import org.afraid.freedns.entity.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户持久层，使用通用mapper(https://github.com/abel533/Mapper)
 * 
 * @author jianggq
 * @date 2018年7月24日下午7:34:26
 */
public interface UserMapper extends Mapper<User> {
}