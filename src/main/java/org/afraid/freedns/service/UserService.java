package org.afraid.freedns.service;

import org.afraid.freedns.dto.UserDto;
import org.afraid.freedns.dto.UserLoginDto;
import org.afraid.freedns.dto.UserUpdateDto;

public interface UserService {

	/**
	 * 因为是demo,所以只获取一条数据库数据
	 * 
	 * @return
	 */
	public UserDto getOnlyOne();

	/**
	 * 代理登录
	 * 
	 * @param loginDto
	 * @return
	 */
	public UserDto login(UserLoginDto loginDto);

	/**
	 * 用户信息获取并存储
	 * 
	 * @return
	 */
	public UserDto profile(UserLoginDto loginDto);

	/**
	 * 用户信息修改
	 * 
	 * @param updateDto
	 * @return
	 */
	public UserDto update(UserUpdateDto updateDto);

	/**
	 * 用户信息删除
	 * 
	 * @param updateDto
	 * @return
	 */
	public int delete(Long id);
}
