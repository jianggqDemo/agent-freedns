package org.afraid.freedns.service.impl;

import java.util.Date;

import org.afraid.freedns.api.FreednsAPI;
import org.afraid.freedns.dto.UserDto;
import org.afraid.freedns.dto.UserLoginDto;
import org.afraid.freedns.dto.UserUpdateDto;
import org.afraid.freedns.entity.User;
import org.afraid.freedns.mapper.UserMapper;
import org.afraid.freedns.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 代理用户信息操作服务
 * 
 * @author jianggq
 * @date 2018年7月24日下午6:50:33
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

	private final UserMapper userMapper;

	@Autowired
	public UserServiceImpl(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	/**
	 * 因为是demo,所以只获取一条数据库数据
	 * 
	 * @return
	 */
	@Override
	public UserDto getOnlyOne() {
		User entity = userMapper.selectOne(new User());

		if (entity == null)
			return null;

		UserDto dto = new UserDto();
		dto.setId(entity.getId());
		dto.setAccount(entity.getAccount());
		dto.setName(entity.getName());
		dto.setPwd(entity.getPwd());
		dto.setEmail(entity.getEmail());
		dto.setUpdateTime(entity.getUpdateTime());
		dto.setCreateTime(entity.getCreateTime());

		return dto;
	}

	/**
	 * 代理登录
	 * 
	 * @param loginDto
	 * @return
	 */
	@Override
	public UserDto login(UserLoginDto loginDto) {

		// 登录
		FreednsAPI.login(loginDto);
		log.info("登录成功，{}", loginDto.getEmail());

		// 用户信息获取
		UserDto userDto = profile(loginDto);
		log.info("用户信息获取成功，{}", userDto.toString());

		return userDto;
	}

	/**
	 * 用户信息获取并存储
	 * 
	 * @return
	 */
	@Override
	public UserDto profile(UserLoginDto loginDto) {

		UserDto dto = FreednsAPI.profile();

		User record = new User();
		record.setId(dto.getId());
		record.setAccount(dto.getAccount());
		record.setName(dto.getName());
		record.setPwd(loginDto.getPwd());
		record.setEmail(dto.getEmail());
		record.setUpdateTime(dto.getUpdateTime());
		record.setCreateTime(dto.getCreateTime());

		// 每次获取信息后更新一下数据库，保持和http://freedns.afraid.org网站数据库同步。
		User queryRecord = new User();
		queryRecord.setAccount(dto.getAccount());

		User user = userMapper.selectOne(queryRecord);

		if (user == null) {
			userMapper.insertSelective(record); // TODO 这里就不判断result影响行数了，不抛异常默认成功
		} else {
			record.setId(user.getId());
			record.setCreateTime(null);

			userMapper.updateByPrimaryKeySelective(record);// TODO 这里就不判断result影响行数了，不抛异常默认成功
		}

		dto.setId(record.getId()); // 插入或更新后，赋值id
		dto.setPwd(loginDto.getPwd());

		return dto;
	}

	/**
	 * 用户信息修改
	 * 
	 * @param updateDto
	 * @return
	 */
	public UserDto update(UserUpdateDto updateDto) {

		User record = new User();
		record.setId(updateDto.getId());
		record.setAccount(updateDto.getAccount());
		record.setName(updateDto.getName());
		record.setPwd(updateDto.getPwd());
		record.setEmail(updateDto.getEmail());
		record.setUpdateTime(new Date());

		userMapper.updateByPrimaryKeySelective(record);

		// 更新成功后,返回新数据
		UserDto dto = new UserDto();
		dto.setId(updateDto.getId());
		dto.setAccount(updateDto.getAccount());
		dto.setName(updateDto.getName());
		dto.setPwd(updateDto.getPwd());
		dto.setEmail(updateDto.getEmail());
		dto.setUpdateTime(record.getUpdateTime());
		dto.setCreateTime(updateDto.getCreateTime());

		return dto;

	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		User record = new User();
		record.setId(id);
		return userMapper.delete(record);
	}
}
