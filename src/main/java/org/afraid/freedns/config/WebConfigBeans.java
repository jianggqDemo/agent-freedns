package org.afraid.freedns.config;

import javax.annotation.PostConstruct;

import org.afraid.freedns.component.StringToDateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

/**
 * bean配置类
 * 
 * @author jianggq
 * @date 2018年7月24日下午9:00:22
 */
@Configuration
public class WebConfigBeans {

	@Autowired
	RequestMappingHandlerAdapter requestMappingHandlerAdapter;

	@PostConstruct
	public void initEditableValidation() {
		ConfigurableWebBindingInitializer configurableWebBindingInitializer = (ConfigurableWebBindingInitializer) requestMappingHandlerAdapter
				.getWebBindingInitializer();

		// 添加表单提交时日期类型的转换
		if (configurableWebBindingInitializer.getConversionService() != null) {
			GenericConversionService service = (GenericConversionService) configurableWebBindingInitializer.getConversionService();
			service.addConverter(new StringToDateConverter());
		}
	}
}