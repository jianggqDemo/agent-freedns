package org.afraid.freedns.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户登录实体
 * 
 * @author jianggq
 * @date 2018年7月24日下午6:45:51
 */
@Getter
@Setter
public class UserLoginDto {

	@NotBlank(message = "用户邮箱不能为空")
	@Email(message = "邮箱格式不正确")
	private String email;

	@NotBlank(message = "用户密码不能为空")
	private String pwd;
}
