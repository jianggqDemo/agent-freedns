package org.afraid.freedns.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 用户实体
 * 
 * @author jianggq
 * @date 2018年7月24日下午6:23:48
 */
@Getter
@Setter
@ToString
public class UserDto {

	/**
	 * 用户唯一标识
	 */
	private Long id;

	/**
	 * 用户账号
	 */
	private String account;

	/**
	 * 用户名称
	 */
	private String name;

	/**
	 * 用户密码
	 */
	private String pwd;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 创建时间
	 */
	private Date updateTime;

	/**
	 * 创建时间
	 */
	private Date createTime;

}
