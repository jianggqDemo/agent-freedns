package org.afraid.freedns.dto;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户更新实体
 * 
 * @author jianggq
 * @date 2018年7月24日下午6:23:48
 */
@Getter
@Setter
public class UserUpdateDto {

	@NotNull(message = "主键id不能为空")
	@Min(value = 1, message = "主键id大于0")
	private Long id;

	@NotBlank(message = "用户账号不能为空")
	private String account;

	@NotBlank(message = "用户名称不能为空")
	private String name;

	@NotBlank(message = "用户密码不能为空")
	private String pwd;

	@NotBlank(message = "用户邮箱不能为空")
	@Email(message = "邮箱格式不正确")
	private String email;

	// 更新时间
	private Date updateTime;

	// 创建时间
	private Date createTime;

}
