<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>代理服务-信息显示</title>
    </head>

    <body>
        <div class="container form-margin-top">
            <form name="profileForm" class="form-signin" action="${request.contextPath}/user/update" method="post">
            	<h2 class="form-signin-heading" align="center">信息显示</h2>
            	<input type="hidden" name="id" value="${user.id}">
                <input type="text" name="account" class="form-control form-margin-top" placeholder="UserID" required autofocus value="${user.account}">
                <input type="text" name="name" class="form-control" placeholder="Name" required value="${user.name}">
                <input type="text" name="pwd" class="form-control" placeholder="Password" required value="${user.pwd}" readonly>
                <input type="text" name="email" class="form-control" placeholder="E-Mail" required value="${user.email}" readonly>
                <input type="text" name="updateTime" class="form-control" placeholder="UpdateTime" value="${user.updateTime?string('yyyy-MM-dd HH:mm:ss')}" readonly >
                <input type="text" name="createTime" class="form-control" placeholder="CreateTime" value="${user.createTime?string('yyyy-MM-dd HH:mm:ss')}" readonly >
                <button class="btn btn-lg btn-primary btn-block" type="button" onclick="javascript:location.href='${request.contextPath}/user/delete?id=${user.id}';">delete</button>
                <button class="btn btn-lg btn-primary btn-block" type="submit">update</button>
            </form>
        </div>
        <footer>
            <p>author: jianggq</p>
            <p>mobile: <a href=""></a>18911608230</p>
            <p>email: <a href=""></a>jgqwp@163.com</p>
        </footer>
    </body>
</html>
