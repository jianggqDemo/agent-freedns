<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>代理服务-登录</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/signin.css" rel="stylesheet">
    </head>

    <body>
        <div class="container form-margin-top">
            <form class="form-signin" action="${request.contextPath}/user/login" method="POST">
                <h2 class="form-signin-heading" align="center">登录</h2>
                <input type="text" name="email" class="form-control form-margin-top" placeholder="邮箱" required autofocus>
                <input type="password" name="pwd" class="form-control" placeholder="密码" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">sign in</button>
            </form>
            <!-- 登录简单的显示一下异常信息 -->
            <h2><font color="red"><#if errorMsg??>${errorMsg}<#else></#if></font></h2>
        </div>
        <footer>
            <p>author: jianggq</p>
            <p>mobile: <a href=""></a>18911608230</p>
            <p>email: <a href=""></a>jgqwp@163.com</p>
        </footer>
    </body>
</html>
