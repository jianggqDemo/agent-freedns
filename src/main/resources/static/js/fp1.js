function fnSaveInput() {
    var oPersist=oPersistForm.oPersistInput;
    oPersist.setAttribute("sPersist",oPersist.value);
    oPersist.save("oXMLBranch");
}

function fnLoadInput() {
    var oPersist=oPersistForm.oPersistInput;
    oPersist.load("oXMLBranch");
    oPersist.value=oPersist.getAttribute("sPersist");
    return oPersist.value;
}

function fnAlertInput() {
    var oPersist=oPersistForm.oPersistInput;
    oPersist.load("oXMLBranch");
    oPersist.value=oPersist.getAttribute("sPersist");
    if (!oPersist.value || oPersist.value == 'null') {
        // Nothing set.
        oPersist.value=fnGenerateFingerPrint();
        fnSaveInput();
        fnLoadInput();
        oPersistForm.submit();
    } else {
        oPersistForm.submit();
    }
}

function fnGenerateFingerPrint() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz!@#$%^&*()_+=-";
    var string_length = 64;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
}
