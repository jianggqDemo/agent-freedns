## 项目结构说明 https://gitee.com/jianggqDemo/agent-freedns.git

# 警告：运行项目先执行init/tb_init.sql
	
### src/main/java
	
	org.afraid.freedns.api：网络请求协议调用，数据解析封装
	org.afraid.freedns.component：组件：AOP全局异常拦截
	org.afraid.freedns.config：配置管理：可添加自定义Bean，或其它MVC配置
	org.afraid.freedns.constant：常量类
	org.afraid.freedns.controller：控制层
	org.afraid.freedns.dto ：模型实体：对外提供数据需要将entity封装成dto返回；或前端请求的dto需要封装成entity然后执行数据库操作。
	org.afraid.freedns.entity：数据库实体，与数据库表结构一一对应。不允许添加非数据库字段。
	org.afraid.freedns.exception： 自定义异常
	org.afraid.freedns.mapper：持久层，使用通用mapper(https://github.com/abel533/Mapper)
		逆向数据库生成持久层  set MAVEN_OPTS="-Dfile.encoding=UTF-8"，mvn mybatis-generator:generate
	org.afraid.freedns.service：服务层，处理登录和用户信息修改逻辑
	org.afraid.freedns.util：工具类，目前有OKHTTP网络访问工具。

### src/main/resources

	builder：MyBatis自动生成配置
	init：数据库脚本
	mapper：持久xml
	static：css, js
	templates：freemarker ftl
	
## 注意
	1  如果数据库有一条数据库，那么访问http://127.0.0.1:8080/agent-freedns的时候，会自动登录，（不知道是不是文档中提到的“自动登录该Web网站”的需求）
	2. 自动注册就不做了
	3. 高可用使用Redis存储session信息。
	4. 使用 OKHttp 存储了每次登录成功后的网站(http://freedns.afraid.org/)的session,也需要存储到Redis；OKHttp有机制可以这么做。
	5. 全局异常处理等等提升系统稳定性的东西，实在没有时间就不弄了，还要准备面试。
	6. 安全方面也没有太多考虑，登录密码目前没有加密存储等等(主要考虑demo的原因)

## 打包

	mvn clean package -Dmaven.test.skip=true spring-boot:repackage
	
## 启动

	开发环境：java -jar agent-freedns-1.0.0-RELEASE.jar --Dspring.profiles.active=dev
  	生产环境：java -jar agent-freedns-1.0.0-RELEASE.jar --Dspring.profiles.active=prd
  	
## 访问地址

	http://127.0.0.1:8080/agent-freedns
  	


